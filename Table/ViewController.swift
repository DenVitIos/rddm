//
//  ViewController.swift
//  Table
//
//  Created by Denis on 02.06.2020.
//  Copyright © 2020 Denys Vytryshko. All rights reserved.
//

import UIKit
import ReactiveDataDisplayManager


class ViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    
//    var dataDisplayManager: BaseTableDataDisplayManager!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
//       var dataDisplayManager = BaseTableDataDisplayManager.init(collection: tableView)
        
        let dataDisplayManager = BaseTableDataDisplayManager(collection: tableView)
        
        let familyModel = Family()
        let gen = FamilyCellGenerator(model: familyModel)
        gen.build(view: FamilyTableViewCell())
        gen.show(family: familyModel)
        gen.registerCell(in: tableView)
        
//        dataDisplayManager.addCellGenerator(gen as! TableCellGenerator)
        
//        dataDisplayManager.addCellGenerator(gen as! TableCellGenerator)
//        self.dataDisplayManager.addCellGenerator(gen)
        
        
        
    }
}

