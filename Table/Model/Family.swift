//
//  Family.swift
//  Table
//
//  Created by Denis on 11.06.2020.
//  Copyright © 2020 Denys Vytryshko. All rights reserved.
//

import Foundation

struct Family{
    var mom: String? = "Bazz"
    var dad: String? = "Bar"
    var bro: String? = "Bar"
}
